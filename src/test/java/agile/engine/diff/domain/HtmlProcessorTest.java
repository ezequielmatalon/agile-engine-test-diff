package agile.engine.diff.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import agile.engine.diff.exception.ElementNotFound;
import agile.engine.diff.exception.FileUnavailable;
import agile.engine.diff.exception.SourceElementDoesNotExist;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class HtmlProcessorTest {

  private static final String ATTRIBUTE_KEY = "class";
  private static final String ATTRIBUTE_VALUE = "pln";
  private static final String ATTRIBUTE_KEY2 = "data-something";
  private static final String ATTRIBUTE_VALUE2 = "something";

  HtmlProcessor processor;

  String testFilePath;

  @Mock
  Document doc;

  Element element;



  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);
    processor = new HtmlProcessor();
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource("exampleFiles/sample-0-origin.html")
        .getFile());
    testFilePath = file.getAbsolutePath();
    element = Jsoup
        .parseBodyFragment("<span " + ATTRIBUTE_KEY + "=\"" + ATTRIBUTE_VALUE + "\" "
            + ATTRIBUTE_KEY2 + "=\"" + ATTRIBUTE_VALUE2 + "\"> text </span>")
        .body()
        .selectFirst("span");
  }

  @Test
  public void testReadDocument() {
    Document document = processor.readDocument(testFilePath);
    assertNotNull(document);
  }

  @Test(expected = FileUnavailable.class)
  public void testFailReadDocument() {
    processor.readDocument("RANDOM_STRING");
  }

  @Test
  public void testGetElementAttributesById() {
    when(doc.getElementById(anyString())).thenReturn(element);

    List<Attribute> listOfAttributes = processor.getElementAttributesById("id", doc);

    assertNotNull(listOfAttributes);
    assertEquals(listOfAttributes.size(), 2);

    Attribute attr = listOfAttributes.get(0);
    assertEquals(attr.getKey(), ATTRIBUTE_KEY);
    assertEquals(attr.getValue(), ATTRIBUTE_VALUE);
  }

  @Test(expected = SourceElementDoesNotExist.class)
  public void testFailGetElementAttributesById() {
    when(doc.getElementById(anyString())).thenReturn(null);

    processor.getElementAttributesById("id", doc);
  }

  @Test
  public void testMostLikelyElement() {
    when(doc.select(anyString())).thenReturn(new Elements(Arrays.asList(element)));

    String mostLikelyElement = processor.mostLikelyElement(doc, element.attributes().asList());
    assertNotNull(mostLikelyElement);

    String elementClass = mostLikelyElement.substring(mostLikelyElement.lastIndexOf(".") + 1);
    assertTrue(elementClass.equals(ATTRIBUTE_VALUE));

    verify(doc, times(2)).select(anyString());
  }

  @Test(expected = ElementNotFound.class)
  public void testFailMostLikelyElement() {
    when(doc.select(anyString())).thenReturn(null);

    processor.mostLikelyElement(doc, element.attributes().asList());

    verify(doc, times(1)).select(anyString());
  }
}