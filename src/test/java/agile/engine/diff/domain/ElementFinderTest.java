package agile.engine.diff.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import agile.engine.diff.exception.ElementNotFound;
import agile.engine.diff.exception.FileUnavailable;
import agile.engine.diff.exception.SourceElementDoesNotExist;
import java.util.List;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ElementFinderTest {

  private static final String PATH_RESPONSE = "a > path > to > element";

  @Mock
  private HtmlProcessor htmlProcessor;

  @InjectMocks
  ElementFinder elementFinder;

  @Mock
  Document doc;

  @Mock
  List<Attribute> attributes;

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testFindSpecificElementWithId() {

    when(htmlProcessor.readDocument(anyString()))
        .thenReturn(doc);
    when(htmlProcessor.getElementAttributesById(anyString(), any(Document.class)))
        .thenReturn(attributes);
    when(htmlProcessor.mostLikelyElement(any(Document.class), anyList()))
        .thenReturn(PATH_RESPONSE);

    String element = elementFinder.findSpecificElement("a", "b", "c");

    assertNotNull(element);
    assertEquals(element, PATH_RESPONSE);

    verify(htmlProcessor, times(2))
        .readDocument(anyString());
    verify(htmlProcessor, times(1))
        .getElementAttributesById(anyString(), any(Document.class));
    verify(htmlProcessor, times(1))
        .mostLikelyElement(any(Document.class), anyList());

  }

  @Test
  public void testFindSpecificElement() {
    when(htmlProcessor.readDocument(anyString()))
        .thenReturn(doc);
    when(htmlProcessor.getElementAttributesById(anyString(), any(Document.class)))
        .thenReturn(attributes);
    when(htmlProcessor.mostLikelyElement(any(Document.class), anyList()))
        .thenReturn(PATH_RESPONSE);

    String element = elementFinder.findSpecificElement("a", "b");

    assertNotNull(element);
    assertEquals(element, PATH_RESPONSE);

    verify(htmlProcessor, times(2))
        .readDocument(anyString());
    verify(htmlProcessor, times(1))
        .getElementAttributesById(anyString(), any(Document.class));
    verify(htmlProcessor, times(1))
        .mostLikelyElement(any(Document.class), anyList());

  }

  @Test(expected = FileUnavailable.class)
  public void testFailByInexistentFileFindSpecificElement() {
    when(htmlProcessor.readDocument(anyString())).thenThrow(FileUnavailable.class);

    elementFinder.findSpecificElement("a", "b", "c");

    verify(htmlProcessor.readDocument(anyString()), times(1));
    verify(htmlProcessor.getElementAttributesById(anyString(), any(Document.class)),
        times(0));
    verify(htmlProcessor.mostLikelyElement(any(Document.class), anyList()),
        times(0));
  }

  @Test(expected = SourceElementDoesNotExist.class)
  public void testFailBySourceElementNotPresentFindSpecificElement() {
    when(htmlProcessor.readDocument(anyString()))
        .thenReturn(doc);
    when(htmlProcessor.getElementAttributesById(anyString(), any(Document.class)))
        .thenThrow(SourceElementDoesNotExist.class);

    elementFinder.findSpecificElement("a", "b", "c");

    verify(htmlProcessor.readDocument(anyString()), times(2));
    verify(htmlProcessor.getElementAttributesById(anyString(), any(Document.class)),
        times(1));
    verify(htmlProcessor.mostLikelyElement(any(Document.class), anyList()),
        times(0));
  }


  @Test(expected = ElementNotFound.class)
  public void testFailByElementNotFoundFindSpecificElement() {
    when(htmlProcessor.readDocument(anyString()))
        .thenReturn(doc);
    when(htmlProcessor.getElementAttributesById(anyString(), any(Document.class)))
        .thenReturn(attributes);
    when(htmlProcessor.mostLikelyElement(any(Document.class), anyList()))
        .thenThrow(ElementNotFound.class);

    elementFinder.findSpecificElement("a", "b", "c");

    verify(htmlProcessor, times(2))
        .readDocument(anyString());
    verify(htmlProcessor, times(1))
        .getElementAttributesById(anyString(), any(Document.class));
    verify(htmlProcessor,
        times(1))
        .mostLikelyElement(any(Document.class), anyList());
  }


}