package agile.engine.diff.domain;

import static org.junit.Assert.assertEquals;

import java.io.File;
import org.junit.Before;
import org.junit.Test;

public class ElementFinderIT {

  private String origin;
  private String case1;
  private String case2;
  private String case3;
  private String case4;
  private ElementFinder elementFinder;

  @Before
  public void init() {
    ClassLoader classLoader = getClass().getClassLoader();

    origin = new File(classLoader.getResource("exampleFiles/sample-0-origin.html")
        .getFile()).getAbsolutePath();
    case1 = new File(classLoader.getResource("exampleFiles/sample-1-evil-gemini.html")
        .getFile()).getAbsolutePath();

    case2 = new File(classLoader.getResource("exampleFiles/sample-2-container-and-clone.html")
        .getFile()).getAbsolutePath();

    case3 = new File(classLoader.getResource("exampleFiles/sample-3-the-escape.html")
        .getFile()).getAbsolutePath();

    case4 = new File(classLoader.getResource("exampleFiles/sample-4-the-mash.html")
        .getFile()).getAbsolutePath();


    elementFinder = new ElementFinder();

  }

  @Test
  public void findSpecificElementCase1() {
    String element = elementFinder.findSpecificElement(origin, case1);
    assertEquals(element, "#page-wrapper > div.row:nth-child(3) > div.col-lg-8 > " +
        "div.panel.panel-default > div.panel-body > a.btn.btn-success");
  }

  @Test
  public void findSpecificElementCase2() {
    String element = elementFinder.findSpecificElement(origin, case2);
    assertEquals(element, "#page-wrapper > div.row:nth-child(3) > div.col-lg-8 >" +
        " div.panel.panel-default > div.panel-body > div.some-container > a.btn.test-link-ok");
  }

  @Test
  public void findSpecificElementCase3() {
    String element = elementFinder.findSpecificElement(origin, case3);
    assertEquals(element, "#page-wrapper > div.row:nth-child(3) > " +
        "div.col-lg-8 > div.panel.panel-default > div.panel-footer > a.btn.btn-success");
  }

  @Test
  public void findSpecificElementCase4() {
    String element = elementFinder.findSpecificElement(origin, case4);
    assertEquals(element, "#page-wrapper > div.row:nth-child(3) > " +
        "div.col-lg-8 > div.panel.panel-default > div.panel-footer > a.btn.btn-success");
  }
}
