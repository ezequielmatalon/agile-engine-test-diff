package agile.engine.diff.exception;

public class FileUnavailable extends RuntimeException {

  public FileUnavailable(String filepath) {
    super(String.format("The file %s is unavailable or does not exist", filepath));
  }
}
