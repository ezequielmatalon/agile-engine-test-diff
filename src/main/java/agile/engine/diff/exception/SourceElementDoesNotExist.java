package agile.engine.diff.exception;

public class SourceElementDoesNotExist extends RuntimeException {

  public SourceElementDoesNotExist(String id) {
    super(String.format("The element with id %s could not be found on the document", id));
  }
}