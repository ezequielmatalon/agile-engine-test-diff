package agile.engine.diff.exception;

public class ElementNotFound extends RuntimeException {

  public ElementNotFound() {
    super("Could not find any similar elements with the provided attributes");
  }
}
