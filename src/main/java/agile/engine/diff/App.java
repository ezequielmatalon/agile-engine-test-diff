package agile.engine.diff;

import agile.engine.diff.domain.ElementFinder;
import org.jsoup.helper.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
  private static Logger LOGGER = LoggerFactory.getLogger(App.class);

  public static void main(String[] args) {
    ElementFinder elementAnalyzer = new ElementFinder();
    String originalPath = args[0];
    String newPath = args[1];
    String id = args.length > 2 ? args[2] : null;

    String element;
    if (!StringUtil.isBlank(id)) {
      element = elementAnalyzer.findSpecificElement(originalPath, newPath, id);
    } else {
      element = elementAnalyzer.findSpecificElement(originalPath, newPath);
    }
    LOGGER.info("Path to the element [{}]", element);
  }
}
