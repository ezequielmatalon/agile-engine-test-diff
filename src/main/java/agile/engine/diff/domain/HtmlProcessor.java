package agile.engine.diff.domain;

import agile.engine.diff.exception.ElementNotFound;
import agile.engine.diff.exception.FileUnavailable;
import agile.engine.diff.exception.SourceElementDoesNotExist;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HtmlProcessor {
  private static final Logger LOGGER = LoggerFactory.getLogger(HtmlProcessor.class);
  private static final String CHARSET_NAME = "UTF-8";

  /**
   * Reads filepath and returns object representation of the document
   *
   * @param pathToFile filepath.
   * @return Object representation of the document.
   */
  public Document readDocument(String pathToFile) {
    File file = new File(pathToFile);
    try {
      Document doc = Jsoup.parse(file,
          CHARSET_NAME,
          file.getAbsolutePath());
      return doc;
    } catch (IOException e) {
      LOGGER.error("Error reading [{}] file", file.getAbsolutePath(), e);
      throw new FileUnavailable(file.getAbsolutePath());
    }
  }

  /**
   * Searches on a document object an element with the specified id
   * and returns all of its attributes.
   *
   * @param id       the element id.
   * @param document the document.
   * @return the attribute list for the element.
   */
  public List<Attribute> getElementAttributesById(String id, Document document) {
    Optional<Element> optionalOriginalElement = findElementById(document, id);
    Element originalElement = optionalOriginalElement
        .orElseThrow(() -> new SourceElementDoesNotExist(id));

    return originalElement.attributes().asList();
  }

  /**
   * Tries to find an alike element based on its attributes on a document.
   * Elements are grouped by their Xpath and sorted by counting in order
   * to return the element that matches with most of the attributes.
   *
   * @param document      the document.
   * @param attributeList list of attributes of the former object.
   * @return element Xpath.
   */
  public String mostLikelyElement(Document document, List<Attribute> attributeList) {
    Map<String, Long> sortByCountingElements = attributeList.stream()
        .map(at -> findElementsByQuery(document, getCssQuery(at)).orElse(null))
        .filter(Objects::nonNull)
        .flatMap(elements -> elements.stream())
        .collect(Collectors.groupingBy(element -> element.cssSelector(), Collectors.counting()));

    Optional<Map.Entry<String, Long>> maxCoincidenceElement = sortByCountingElements.entrySet()
        .stream()
        .max((a, b) -> (int) (a.getValue() - b.getValue()));

    if (!maxCoincidenceElement.isPresent()) {
      LOGGER.error("Element is not present on the current document %s", document.title());
      throw new ElementNotFound();
    }
    return maxCoincidenceElement.get().getKey();
  }


  private String getCssQuery(Attribute at) {
    return "*[" + at.getKey() + "=" + at.getValue() + "]";
  }

  private Optional<Element> findElementById(Document doc, String targetElementId) {
    return Optional.ofNullable(doc.getElementById(targetElementId));

  }

  private Optional<Elements> findElementsByQuery(Document doc, String cssQuery) {
    return Optional.ofNullable(doc.select(cssQuery));
  }
}
