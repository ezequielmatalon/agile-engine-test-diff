package agile.engine.diff.domain;

import java.util.List;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;

public class ElementFinder {

  private HtmlProcessor htmlProcessor = new HtmlProcessor();

  /**
   * ElementFinder#findSpecificElement(String, String, String) with "make-everything-ok-button".
   *
   * @param sourceFilePath source filepath.
   * @param targetFilePath target filepath.
   * @return Similar object xpath.
   * @see ElementFinder#findSpecificElement(String, String, String)
   */
  public String findSpecificElement(String sourceFilePath,
                                    String targetFilePath) {
    return findSpecificElement(sourceFilePath, targetFilePath, "make-everything-ok-button");
  }

  /**
   * Searches for an element on the source document based on an id
   * and tries to find a similar one on the target document based on its attributes and return
   * its Xpath.
   *
   * @param sourceFilePath source filepath.
   * @param targetFilePath target filepath.
   * @param id             element id.
   * @return Similar object xpath.
   */
  public String findSpecificElement(String sourceFilePath,
                                    String targetFilePath,
                                    String id) {
    Document originalDocument = htmlProcessor.readDocument(sourceFilePath);
    Document newDocument = htmlProcessor.readDocument(targetFilePath);
    List<Attribute> attributeList = htmlProcessor.getElementAttributesById(id, originalDocument);
    return htmlProcessor.mostLikelyElement(newDocument, attributeList);
  }


}