**Execution instructions**

You can either execute text-diff-1.0.jar providing the classpath or just run
text-diff-1.0-jar-with-dependencies.jar that already includes the dependencies.
Id is included as last parameter and it is optional. The default value
is the stated on the exercise.

**Base Cases**

Input:
`{path+"sample-0-origin.html", path+"sample-1-evil-gemini.html", "make-everything-ok-button"}
`
Output:
`[INFO] 2018-05-21 14:42:49,727 a.e.d.Diff - Path to the element [#page-wrapper > div.row:nth-child(3) > div.col-lg-8 > div.panel.panel-default > div.panel-body > a.btn.btn-success]
`

Input:

`{path+"sample-0-origin.html", path+"sample-2-container-and-clone.html", "make-everything-ok-button"}`


Output:

`[INFO] 2018-05-21 14:45:43,984 a.e.d.Diff - Path to the element [#page-wrapper > div.row:nth-child(3) > div.col-lg-8 > div.panel.panel-default > div.panel-body > div.some-container > a.btn.test-link-ok]
`

Input:

`{path+"sample-0-origin.html", path+"sample-3-the-escape.html", "make-everything-ok-button"}`

Output:

`[INFO] 2018-05-21 14:47:27,579 a.e.d.Diff - Path to the element [#page-wrapper > div.row:nth-child(3) > div.col-lg-8 > div.panel.panel-default > div.panel-footer > a.btn.btn-success]
`

Input:

`{path+"sample-0-origin.html", path+"sample-4-the-mash.html", "make-everything-ok-button"}`

Output:

`[INFO] 2018-05-21 14:48:51,386 a.e.d.Diff - Path to the element [#page-wrapper > div.row:nth-child(3) > div.col-lg-8 > div.panel.panel-default > div.panel-footer > a.btn.btn-success]
`
